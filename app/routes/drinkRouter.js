const express = require("express");

const {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} = require("../middlewares/drinkMiddleware.js");

const drinkRouter = express.Router();

drinkRouter.get("/drinks", getAllDrinkMiddleware, (req,res) => {
    res.status(200).json({
        message: "Get all drinks"
    })
})

drinkRouter.get("/drinks/:drinkId", getDrinkMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.status(200).json({
        message: `Get a drinks with drinkId = ${drinkId}`
    })
})

drinkRouter.post("/drinks", postDrinkMiddleware, (req,res) => {
    res.status(201).json({
        message: "Create a drink"
    })
})

drinkRouter.put("/drinks/:drinkId", putDrinkMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.status(200).json({
        message: `Update a drinks with drinkId = ${drinkId}`
    })
})

drinkRouter.delete("/drinks/:drinkId", deleteDrinkMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.status(204).json({
        message: `Delete a drinks with drinkId = ${drinkId}`
    })
})

module.exports = drinkRouter;

