const express = require("express");

const {
    getAllOrderMiddleware,
    getOrderMiddleware,
    postOrderMiddleware,
    putOrderMiddleware,
    deleteOrderMiddleware
} = require("../middlewares/orderMiddleware.js");

const orderRouter = express.Router();

orderRouter.get("/orders", getAllOrderMiddleware, (req,res) => {
    res.status(200).json({
        message: "Get all orders"
    })
})

orderRouter.get("/orders/:orderId", getOrderMiddleware, (req,res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Get a orders with orderId = ${orderId}`
    })
})

orderRouter.post("/orders", postOrderMiddleware, (req,res) => {
    res.status(201).json({
        message: "Create a order"
    })
})

orderRouter.put("/orders/:orderId", putOrderMiddleware, (req,res) => {
    let orderId = req.params.orderId;
    res.status(200).json({
        message: `Update a orders with orderId = ${orderId}`
    })
})

orderRouter.delete("/orders/:orderId", deleteOrderMiddleware, (req,res) => {
    let orderId = req.params.orderId;
    res.status(204).json({
        message: `Delete a orders with orderId = ${orderId}`
    })
})

module.exports = orderRouter;

