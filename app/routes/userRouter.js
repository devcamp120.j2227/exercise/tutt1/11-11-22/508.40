const express = require("express");

const {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
} = require("../middlewares/userMiddleware.js");

const userRouter = express.Router();

userRouter.get("/users", getAllUserMiddleware, (req, res) => {
    res.status(200).json({
        message: "Get all users"
    })
})

userRouter.get("/users/:userid", getUserMiddleware, (req, res) => {
    let userid = req.params.userid;
    res.status(200).json({
        message: `Get a drinks with userid = ${userid}`
    })
})

userRouter.post("/users", postUserMiddleware, (req, res) => {
    res.status(201).json({
        message: "Create a user"
    })
})

userRouter.put("/users/:userid", putUserMiddleware, (req, res) => {
    let userid = req.params.userid;
    res.status(200).json({
        message: `Update a users with userid = ${userid}`
    })
})

userRouter.delete("/users/:userid", deleteUserMiddleware, (req, res) => {
    let userid = req.params.userid;
    res.status(204).json({
        message: `Delete a users with userid = ${userid}`
    })
})

module.exports = userRouter;