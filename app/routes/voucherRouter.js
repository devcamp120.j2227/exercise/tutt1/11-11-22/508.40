const express = require("express");

const {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
} = require("../middlewares/voucherMiddleware.js");

const voucherRouter = express.Router();

voucherRouter.get("/vouchers", getAllVoucherMiddleware, (req, res) => {
    res.status(200).json({
        message: "Get all vouchers"
    })
})

voucherRouter.get("/vouchers/:voucherid", getVoucherMiddleware, (req, res) => {
    let voucherid = req.params.voucherid;
    res.status(200).json({
        message: `Get a drinks with voucherid = ${voucherid}`
    })
})

voucherRouter.post("/vouchers", postVoucherMiddleware, (req, res) => {
    res.status(201).json({
        message: "Create a voucher"
    })
})

voucherRouter.put("/vouchers/:voucherid", putVoucherMiddleware, (req, res) => {
    let voucherid = req.params.voucherid;
    res.status(200).json({
        message: `Update a vouchers with voucherid = ${voucherid}`
    })
})

voucherRouter.delete("/vouchers/:voucherid", deleteVoucherMiddleware, (req, res) => {
    let voucherid = req.params.voucherid;
    res.status(204).json({
        message: `Delete a vouchers with voucherid = ${voucherid}`
    })
})

module.exports = voucherRouter;