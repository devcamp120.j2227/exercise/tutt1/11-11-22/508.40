const getAllUserMiddleware = (req,res,next) => {
    console.log("Get all users");

    next();
}

const getUserMiddleware = (req,res,next) => {
    console.log("Get a user");

    next();
}

const postUserMiddleware = (req,res,next) => {
    console.log("create a user");

    next();
}

const putUserMiddleware = (req,res,next) => {
    console.log("Update a user");

    next();
}

const deleteUserMiddleware = (req,res,next) => {
    console.log("Delete a user");

    next();
}

module.exports = {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
}