const getAllVoucherMiddleware = (req,res,next) => {
    console.log("Get all vouchers");

    next();
}

const getVoucherMiddleware =(req,res,next) => {
    console.log("Get a voucher");

    next();
}

const postVoucherMiddleware =(req,res, next) => {
    console.log("create a voucher");

    next();
}

const putVoucherMiddleware = (req,res, next) => {
    console.log("Update a voucher");

    next();
}

const deleteVoucherMiddleware = (req,res, next) => {
    console.log("Delete a voucher");

    next();
}

module.exports = {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
}