const getAllDrinkMiddleware = (req,res,next) => {
    console.log("Get all drinks");

    next();
}

const getDrinkMiddleware = (req,res,next) => {
    console.log("Get a drink");

    next();
}

const postDrinkMiddleware = (req,res,next) => {
    console.log("Create a drink");

    next();
}

const putDrinkMiddleware = (req,res,next) => {
    console.log("Update a drink");

    next();
}

const deleteDrinkMiddleware = (req,res,next) => {
    console.log("Delete a drink");

    next();
}

module.exports = {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
}