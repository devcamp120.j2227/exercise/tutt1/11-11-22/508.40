const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema ({
    fullName : {
        type: String,
        required: true
    },
    email : {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders : [
        {
            type: mongoose.Types.ObjectId,
            ref: "Orders"
        }
    ]
}, {
    timestamps: true
});

module.exports = mongoose.model("Users", userSchema);