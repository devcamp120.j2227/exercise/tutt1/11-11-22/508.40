const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema ({
    orderCode : {
        type: String,
        unique: true
    },
    pizzaSize : {
        type: String,
        required: true
    },
    pizzaType : {
        type: String,
        required: true
    },
    voucher : {
        type: mongoose.Types.ObjectId,
        ref: "Vouchers"
    },
    drink : {
        type: mongoose.Types.ObjectId,
        ref: "Drinks"
    },
    status : {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Orders", orderSchema);
