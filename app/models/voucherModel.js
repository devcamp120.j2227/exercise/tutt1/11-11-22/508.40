const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const voucherSchema = new Schema ({
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia : {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Vouchers", voucherSchema);