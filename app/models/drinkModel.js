const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const drinksSchema = new Schema ({
    maNuocUong : {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong : {
        type: String,
        required: true
    },
    donGia : {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Drinks", drinksSchema);